﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Web;
using System.IO;
using DocumentFormat.OpenXml.Packaging;

using Microsoft.AspNetCore.Http;
using DocumentFormat.OpenXml;
using System.Text.RegularExpressions;
using System.Reflection.Metadata;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace kursovay.Controllers
{
    public class BaseController : Controller
    {
        // GET: /<controller>/
        
        public IActionResult Index()
        {
            
            return View();
        }

        public IActionResult GetVirtualFile()
        {
            if (Request.Method.Equals("POST") && !Request.Form["data_textarea"].Equals(""))
            {
                var mimeType = System.Net.Mime.MediaTypeNames.Application.Octet;
                string file_name = "data.docx";

                using MemoryStream fs = new MemoryStream();
                using (WordprocessingDocument wordDocument = WordprocessingDocument.Create(fs, WordprocessingDocumentType.Document, true))
                {
                    MainDocumentPart mainPart = wordDocument.AddMainDocumentPart();
                    mainPart.Document = new DocumentFormat.OpenXml.Wordprocessing.Document();
                    Body body = mainPart.Document.AppendChild(new Body());
                    Paragraph para = body.AppendChild(new Paragraph());
                    Run run = para.AppendChild(new Run());
                    run.AppendChild(new Text(Request.Form["data_textarea"].ToString()));
                }

                byte[] bytes = fs.ToArray();
                fs.Close();
                return File(bytes, mimeType, file_name);
            }
            return RedirectToAction("Index");
        }

        //[HttpPost]
        public IActionResult GetDataFromForms(IFormFile uploadedFile)
        {
            if (Request.Method.Equals("POST"))
            {
                VigenerСipher vigener = new VigenerСipher();
                List<string[]> error = new List<string[]>();

                string text_file = "";
                string key = "";
                bool flag_input = false;
                bool flag_file = false;
                bool flag_action = false;
                string request_str_key = Request.Form["key_value"];

                if (!Request.Form["data_text"].Equals(""))
                {
                    flag_input = true;
                }

                if (!request_str_key.Equals("") && Regex.IsMatch(request_str_key, "^[А-Яа-я]+$") && !String.IsNullOrWhiteSpace(request_str_key))
                {
                    key = Request.Form["key_value"];
                }
                else
                {
                    error.Add(new string[] { "key_value", "Ключ не корректен" });
                }

                if (Request.Form["uploadedFile"] != "")
                {
                    string[] type_file_name = uploadedFile.FileName.Split(".");

                    if (type_file_name[type_file_name.Length - 1] == "docx")
                    {
                        try { 
                            using (WordprocessingDocument filestream = WordprocessingDocument.Open(uploadedFile.OpenReadStream(), false))
                            {
                                text_file = filestream.MainDocumentPart.Document.Body.InnerText;
                            }
                            flag_file = true;
                        }
                        catch
                        {
                            error.Add(new string[] { "key_value", "Ключ не корректен" });
                        }
                    }
                    else
                    {
                        flag_file = false;
                    }
                }

                if (!flag_file && !flag_input)
                {
                    error.Add(new string[] { "data_text", "Текст в форме не задан" });
                    error.Add(new string[] { "uploadedFile", "Файл отсутствует" });
                    ViewBag.error = error;
                    return View("Index", ViewBag);
                }
               
                try
                {
                    flag_action = bool.Parse(Request.Form["flag_action"]);
                }
                catch
                {
                    error.Add(new string[] { "flag_action", "Ошибка выбора действий с данными"});
                }

                if (error.Count == 0)
                {
                    if (flag_action)
                    {
                        if (flag_input)
                        {
                            string text_input = Request.Form["data_text"];
                            ViewBag.flag_input = true;
                            ViewBag.initial_data_textarea = text_input;
                            ViewBag.input_data = vigener.Encrypt(text_input, "скорпион");
                        }

                        if (flag_file)
                        {
                            ViewBag.initial_data_file = text_file;
                            text_file = vigener.Encrypt(text_file, "скорпион");
                            ViewBag.test = text_file;
                            ViewBag.flag_file = true;
                        }
                    }
                    else
                    {
                        if (flag_input)
                        {
                            string text_input = Request.Form["data_text"];
                            ViewBag.flag_input = true;
                            ViewBag.initial_data_textarea = text_input;
                            ViewBag.input_data = vigener.Decrypt(text_input, key);
                        }

                        if (flag_file)
                        {
                            ViewBag.initial_data_file = text_file;
                            ViewBag.flag_file = true;
                            text_file = vigener.Decrypt(text_file, key);
                            ViewBag.test = text_file;
                        }
                    }
                }
                else
                {
                    ViewBag.error = error;
                }
                return View("Index", ViewBag);
            }
            return RedirectToAction("Index", ViewBag);
        }
    }

    class VigenerСipher
    {
        private char[] alf = new char[] {
                'А','Б','В','Г','Д','Е','Ё','Ж',
                'З','И','Й','К','Л','М','Н','О',
                'П','Р','С','Т','У','Ф','Х','Ц',
                'Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'};
        private int length_alf;

        public VigenerСipher()
        {
            this.length_alf = this.alf.Length;
        }

        private string VigenerRealization(string text, string key, bool encrypting = true)
        {
            key = key.ToUpper();

            string message = "";
            int keyword_index = 0;

            foreach (var item in text)
            {
                char upp_char = Convert.ToChar(item.ToString().ToUpper());
                if (!Array.Exists(this.alf, elm => elm == upp_char))
                {
                    message += item;
                    continue;
                }

                int c;
                bool flag_registr = Char.IsLower(item) ? true : false;

                if (encrypting)
                {
                    c = (Array.IndexOf(this.alf, upp_char) + this.length_alf -
                        Array.IndexOf(this.alf, key[keyword_index])) % this.length_alf;
                }
                else
                {
                    c = (Array.IndexOf(this.alf, upp_char) +
                        Array.IndexOf(this.alf, key[keyword_index])) % this.length_alf;
                }

                message += flag_registr ? this.alf[c].ToString().ToLower() : this.alf[c].ToString();
                keyword_index += 1;

                if (keyword_index == key.Length)
                {
                    keyword_index = 0;
                }
            }
            return message;

        }

        public string Encrypt(string text, string key) => this.VigenerRealization(text, key);
        public string Decrypt(string text, string key) => this.VigenerRealization(text, key, false);
    }
}
